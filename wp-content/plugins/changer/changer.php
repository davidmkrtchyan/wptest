<?php
/**
 * @package Changer-Org
 * @version 1.0
 */
/*
 * Plugin Name: Changer
 * Plugin URI: http://www.vk.com
 * Description: The best changer
 * Armstrong: Changer plugin
 * Author: David Mkrtchyan
 * Version: 1.0
 * Author URI: http://www.vk.com
 *
 * */

$changer_output = "";
$custom_name = "";
$notif_admin = "";

function changer_add_admin_pages()
{
    add_options_page('Changer plugin', 'Changer opt', 8, 'changer', 'changer_options_page');

    db_create();
}

function changer_options_page()
{
    echo "<h3>Changer Options</h3>";

//    add_option('changer_alt_name','Some-alt-name');

    /*Changer products*/
    changer_alt_name();

}


function changer_alt_name()
{
    global $wpdb, $notif_admin;
    $table_changer_alt_name = $wpdb->prefix.changer_alt_name;

    if (isset($_POST['changer_alt_name']))
    {
//        if (function_exists('current_user_can') &&
//            !current_user_can('manage_options') )
//            die ( _e('Hacker?', 'changer') );
//
//        if (function_exists('check_admin_referer')){
//            check_admin_referer('changer_base_setup_form');
//        }

        $changer_alt_name = $_POST['changer_alt_name'];


        $wpdb->insert
        (
            $table_changer_alt_name,
            array('name' => $changer_achanger_alt_namelt_name),
            array( '%s')
        );

    }
    echo "
    <form name='changer_change_alt_name' method='post' action='".$_SERVER['PHP_SELF']."?page=changer&amp;updated=true'>";

    if(function_exists('wp_nonce_field'))
    {
        wp_nonce_field('changer_base_setup_form');
    }

    echo "
    
    <table>
        <tr>
            <td colspan='2'>$notif_admin</td>vk
        </tr>
        <tr>
            <td>Alt name</td>
            <td>
                <input type='text' name='changer_alt_name'>
            </td>
        </tr>
        <tr>
            <td>
                <input type='submit' name='changer_add_product_btn' value='Add'>
            </td>
        </tr>
    </table>
    
    ";
}

function db_create()
{
//    exit('here');
    global $wpdb, $notif_admin;

    $table_name = $wpdb->prefix.changer_alt_name;

    // SQLi here
    $db_create =
        "CREATE TABLE IF NOT EXISTS $table_name (
            id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            created_at TIMESTAMP
    )";

    if ( $wpdb->query($db_create) === TRUE){
        $notif_admin = "";
    }else{
        $notif_admin = "Something went wrong";
    }

}

function changer_select_name()
{
    global $wpdb, $custom_name;
    $table_changer_alt_name = $wpdb->prefix.changer_alt_name;

    // SQLi here
    return $custom_name = $wpdb->get_results(
        "SELECT name 
          FROM $table_changer_alt_name 
          WHERE id = (SELECT max(id) FROM $table_changer_alt_name)"
    );
}

function my_admin_scripts()
{
    global $custom_name;
    changer_select_name();
    wp_enqueue_script('custom', plugin_dir_url(__FILE__).'js/custom.js', array('jquery'), '1.0.0', true);
    $customAltName = array(
        'name' => $custom_name
    );
    wp_localize_script('custom', 'php_vars', $customAltName);
}
add_action('wp_enqueue_scripts', 'my_admin_scripts');

add_action('admin_menu', 'changer_add_admin_pages');
//add_action('init', 'changer_run');

