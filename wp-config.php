<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wptest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3XxF {5=aTe3cd s(6>ic&&lvP}3B4I()/:Jk@*A.Bur{;F1{9i[s.Tdej[nU.}H');
define('SECURE_AUTH_KEY',  'Ud>EU]DY2A_=*rO.yUATc !E;snlLt)M?]4K35ZHT7!&xK* 2WL]>hOkOuJuWJ><');
define('LOGGED_IN_KEY',    '&+vl-qj!%|g!{a K7`V?F;I.j{dZ+=]Y^&?-0VGg,TWgc!qn^mbQ5hX$?p|N*KPx');
define('NONCE_KEY',        'N*x!=QAn_zwUka$N)G(DgRnUV|H+v@nP/C(DMrv_G7K!t?3Ko1>5Y&4YXw7aa/m[');
define('AUTH_SALT',        'GhI7E2#:(oSM-G(OJATj4{&si;ybP2f7lqfHS%0Y `]l=I3sDS_I+>BY_CJ2W2U`');
define('SECURE_AUTH_SALT', 'TjSflQa8Fm.rnRjR:G!0V^=t@BS)pR_jUKJzb2jz~r#Sj<lI}D6v}_)etbbDY}}g');
define('LOGGED_IN_SALT',   '5Z<@I=6+;UkJI9*|D0Cc@Kn0hz:G1&g2;|QYh3FzC>mhw>[fchh 3gq%(g:|>Ws9');
define('NONCE_SALT',       'aPs]Na$E!qWvwl=q$ee]h_zuO[T rukU=vc-hB-+$k.[~#C!G5dMl){*_(/qW#,s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
